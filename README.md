# STM32_CAN_Freertos

<!-- <video src="Video/out.mp4" controls title="Titel"></video> -->

![Demo](Video/out.mp4)

## Testprojekt für das STM32F446RE-Board

Im Projekt wird die Kommunikation zwischen Can1 und Can2 getestet.

## Starten

Um die Ergebnisse der Kommunikation zu sehen, muss ein serieller Terminal mit einer Geschwindigkeit von 115200 gestartet werden:

```bash
screen /dev/ttyACM0 115200
```

```bash
Start CAN1/2 testing:
                                                ==>             Message #1 with ID=0x123 received : "1"
                                                ==>             Message #2 with ID=0x234 received : "2"
                                                ==>             Message #3 with ID=0x123 received : "3"
                                                ==>             Message #4 with ID=0x234 received : "4"
                                                ==>             Message #5 with ID=0x123 received : "5"
                                                ==>             Message #6 with ID=0x234 received : "6"
                                                ==>             Message #7 with ID=0x123 received : "7"
                                                ==>             Message #8 with ID=0x123 received : "8"
                                                ==>             Message #9 with ID=0x234 received : "9"
```


<!-- [def]: "Video/out.mp4" -->
